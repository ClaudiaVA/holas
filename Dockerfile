FROM java:8
ADD hola.jar hola.jar
EXPOSE 8080
CMD ["java", "-jar", "hola.jar"]
